﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AmazonSearch.Models;
using System.Collections;
using System.Net;
using System.Xml;
using System.Globalization;
using System.IO;

namespace AmazonSearch.Controllers
{
    public class SearchController : Controller
    {
        private const string OPERATION = "ItemSearch";
        private const string RESPONSE_GROUP = "ItemAttributes,OfferListings";
        private const string SEARCH_INDEX = "All";
        private const string SERVICE = "AWSECommerceService";
        private const string VERSION = "2013-08-01";
        private const string AWS_NAMESPACE = "http://webservices.amazon.com/" + SERVICE + "/" + VERSION;
        private const string CURRENCY_CODES_NAMESPACE = "http://www.w3.org/2001/XMLSchema";
        private const string CURRENCY_CODES_URL = "http://www.webservicex.net/CurrencyConvertor.asmx?WSDL";

        public ActionResult Index()
        {
            return View();
        }

        // _ProductSearch is called when user clicks the button "Find products".
        // This method:
        //  - finds the list of products based on the entered keyword(s);
        //  - finds the currency codes to populate the drop-down list;
        //  - returns the partial view that includes drop-down list for changing the currency and 
        //    table with search results. 
        public ActionResult _ProductSearch(string keywords)
        {
            List<Product> products = GetProductList(keywords);
            Session["products"] = products;
            ViewBag.Products = products;

            List<SelectListItem> currencyCodes = GetCurrencyCodes();
            Session["currencyCodes"] = currencyCodes;
            ViewBag.currencyCodes = currencyCodes;
            ViewBag.conversionRate = 1;

            return PartialView("_ProductSearch");
        } // End of _ProductSearch

        // ChangeCurrency method is called when user changes the currency in the drop-down list.
        // This method:
        //  - finds the currency that needs to be changed and the currency that the user asked for;
        //  - finds the conversion rate between the two currencies;
        //  - saves the information to the ViewBag so that the table could be updated. 
        public ActionResult ChangeCurrency()
        {
            string fromCurrency = (string) Session["initialCurrency"];
            string toCurrency = ((IEnumerable<SelectListItem>) Session["currencyCodes"])
                .Single(m => m.Value.Equals(Request.Form["currencyCodes"]))
                .Text;

            ViewBag.conversionRate = GetConversionRate(fromCurrency, toCurrency);
            ViewBag.Products = Session["products"];

            ((List<SelectListItem>)Session["currencyCodes"])
                .Single(m => m.Text.Equals(fromCurrency))
                .Selected = false;

            ((List<SelectListItem>)Session["currencyCodes"])
                .Single(m => m.Text.Equals(toCurrency))
                .Selected = true;

            ViewBag.currencyCodes = Session["currencyCodes"];

            return PartialView("_ProductSearch");
        }// End of ChangeCurrency

        // GetProductList is called in the _ProductSearch.
        // This method:
        //  - finds the products on all possible pages of ItemSearch (up to five pages possible)
        //    after signing each request and getting the XmlDocument with response.
        private List<Product> GetProductList(string keywords)
        {
            List<Product> products = new List<Product>();

            for (int i = 1; i <= 5; i++)
            {
                Dictionary<string, string> queryMap = GetQueryMap(keywords, i);
                SignedRequestHelper helper = new SignedRequestHelper(queryMap);
                string signedRequest = helper.SignRequest();
                XmlDocument doc = GetXml(signedRequest);
                List<Product> subProducts = GetPageProductList(doc);
                products.AddRange(subProducts);
            }

            return products;
        } // End of GetProductList

        // GetPageProductList is called in GetProductList.
        // This method:
        //  - reads the XmlDocument given as an argument and returns the list of products on that page;
        //  - find the initial currency of prices as a side project.
        private List<Product> GetPageProductList(XmlDocument doc)
        {
            List<Product> products = new List<Product>();

            XmlNamespaceManager manager = new XmlNamespaceManager(doc.NameTable);
            manager.AddNamespace("ns", AWS_NAMESPACE);
            XmlNodeList itemNodes = doc.SelectNodes("descendant::ns:Item", manager);

            foreach (XmlNode itemNode in itemNodes)
            {
                string itemTitle = "";
                decimal itemPrice = new decimal();
                string itemUrl = "";

                XmlNodeList itemChildNodes = itemNode.ChildNodes;
                foreach (XmlNode itemChildNode in itemChildNodes)
                {
                    string nodeName = itemChildNode.LocalName;
                    switch (nodeName)
                    {
                        case "ItemAttributes":
                            XmlNodeList attrNodes = itemChildNode.ChildNodes;
                            foreach (XmlNode attrNode in attrNodes)
                            {
                                string attrName = attrNode.LocalName;
                                switch (attrName)
                                {
                                    case "Title":
                                        itemTitle = attrNode.FirstChild.Value;
                                        break;
                                }
                            }
                            break;

                        case "Offers":
                            XmlNodeList offersNodes = itemChildNode.ChildNodes;
                            foreach (XmlNode offersNode in offersNodes)
                            {
                                string offersNodeName = offersNode.LocalName;
                                switch (offersNodeName)
                                {
                                    case "Offer":
                                        XmlNodeList offerNodes = offersNode.ChildNodes;
                                        foreach (XmlNode offerNode in offerNodes)
                                        {
                                            string offerNodeName = offerNode.LocalName;
                                            switch (offerNodeName)
                                            {
                                                case "OfferListing":
                                                    XmlNodeList offerListingNodes = offerNode.ChildNodes;
                                                    foreach (XmlNode offerListingNode in offerListingNodes)
                                                    {
                                                        string offerListingNodeName = offerListingNode.LocalName;
                                                        switch (offerListingNodeName)
                                                        {
                                                            case "Price":
                                                                if (Session["initialCurrency"] == null)
                                                                {
                                                                    Session["initialCurrency"] = offerListingNode.ChildNodes[1].InnerText;
                                                                }
                                                                Decimal.TryParse(offerListingNode.LastChild.InnerText.Substring(1), NumberStyles.Any, CultureInfo.InvariantCulture, out itemPrice);
                                                                break;
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                        break;
                                }
                            }
                            break;

                        case "DetailPageURL":
                            itemUrl = itemChildNode.FirstChild.Value;
                            break;
                    }

                }

                products.Add(new Product() { Title = itemTitle, Price = itemPrice, Url = itemUrl });
            }

            return products;
        } // End of GetPageProductList

        // GetConversionRate is called in ChangeCurrency.
        // This method:
        //  - finds the conversion rate between two currencies given as arguments.
        // Rate is found from Yahoo datatable yahoo.finance.xchange.
        private decimal GetConversionRate(string fromCurrency, string toCurrency)
        {
            decimal conversionRate;

            string requestUrl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22"
             + fromCurrency
             + toCurrency
             + "%22)&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

            WebClient client = new WebClient();
            string jsonString = client.DownloadString(requestUrl);
            Newtonsoft.Json.Linq.JObject jObject = Newtonsoft.Json.Linq.JObject.Parse(jsonString);

            conversionRate = Convert.ToDecimal((string)jObject.SelectToken("query.results.rate.Rate"), System.Globalization.CultureInfo.InvariantCulture);

            return conversionRate;
        } // End of GetConversionRate

        // GetCurrencyCodes is called in _ProductSearch
        // This method:
        //  - reads the currency codes from webservicex into SelectListItem List. 
        private List<SelectListItem> GetCurrencyCodes()
        {
            XmlDocument doc = GetXml(CURRENCY_CODES_URL);

            XmlNamespaceManager manager = new XmlNamespaceManager(doc.NameTable);
            manager.AddNamespace("ns", CURRENCY_CODES_NAMESPACE);
            XmlNodeList currNodes = doc.SelectNodes("descendant::ns:enumeration", manager);

            List<SelectListItem> currencyCodes = new List<SelectListItem>();

            for (int i = 0; i < currNodes.Count; i++)
            {
                string currCode = currNodes[i].Attributes["value"].Value;
                if (currCode.Equals(Session["initialCurrency"]))
                {
                    currencyCodes.Add(new SelectListItem { Text = currCode, Value = i.ToString(), Selected = true });
                }
                else
                {
                    currencyCodes.Add(new SelectListItem { Text = currCode, Value = i.ToString() });
                }
            }

            currencyCodes = currencyCodes.OrderBy(o => o.Text).ToList();

            return currencyCodes;
        } // End of GetCurrencyCodes

        // GetQueryMap 
        // This method:
        //  - creates a new map with query parameter-value pairs.
        // The map is completed by SignedRequestHelper that holds the associate tag, public key, and secret key.
        private Dictionary<string, string> GetQueryMap(string keywords, int pageNumber)
        {
            Dictionary<string, string> queryMap = new Dictionary<string, string>
            {
                {"Operation", OPERATION},
                {"ResponseGroup", RESPONSE_GROUP},
                {"SearchIndex", SEARCH_INDEX},
                {"Service", SERVICE},
                {"Version", VERSION},
                {"Keywords", keywords},
                {"ItemPage", pageNumber.ToString()}
            };

            return queryMap;
        } // End of GetQueryMap

        // GetXml
        // This method:
        //  - returns the XmlDocument that contains the response from a request url given as an argument.
        private XmlDocument GetXml(string requestUrl)
        {
            WebRequest request = HttpWebRequest.Create(requestUrl);
            WebResponse response = request.GetResponse();
            XmlDocument doc = new XmlDocument();
            doc.Load(response.GetResponseStream());

            return doc;
        }// End of GetXml

    }
}
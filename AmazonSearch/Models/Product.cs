﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AmazonSearch.Models
{
    public class Product
    {
        public string Title { get; set; }

        public decimal Price { get; set; }

        public string Url { get; set; }
    }
}
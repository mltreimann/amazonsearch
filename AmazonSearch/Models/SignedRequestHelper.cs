﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace AmazonSearch.Models
{
    // SignedRequestHelper is a class with methods that help to sign the request.
    public class SignedRequestHelper
    {
        private const string ASSOCIATE_TAG = "";
        private const string AWS_ACCESS_KEY_ID = "";
        private const string AWS_SECRET_KEY = "";

        Dictionary<string, string> queryMap;

        // Constructor
        public SignedRequestHelper(Dictionary<string, string> queryMap)
        {
            this.queryMap = queryMap;
        } // End of SignedRequestHelper

        // SignRequest 
        public string SignRequest()
        {
            string requestBeginning = "http://webservices.amazon.com/onca/xml?";

            string preparedQueryString = PrepareQueryString(this.queryMap);

            string hash = FindHash(preparedQueryString);
            string signature = Uri.EscapeDataString(hash);

            string signedRequest = requestBeginning + preparedQueryString + "&Signature=" + signature;

            return signedRequest;
        } // End of SignRequest

        // FindHash
        // This method:
        //  - finds the HMAC of the query string using SHA256 algorithm.
        public string FindHash(string preparedQueryString)
        {
            string preparedForHashing = PrepareForHashing(preparedQueryString);
            byte[] stringToHash = Encoding.UTF8.GetBytes(preparedForHashing);

            byte[] key = Encoding.UTF8.GetBytes(AWS_SECRET_KEY);
            HMAC hmac = new HMACSHA256(key);

            byte[] hashValue = hmac.ComputeHash(stringToHash);
            string hash = Convert.ToBase64String(hashValue);

            return hash;
        } // End of FindHash

        // PrepareForHashing
        // This method:
        //  - prepends to the query string three lines that are necessary for hashing.
        public string PrepareForHashing(string queryString)
        { 
            string stringToHash = "GET\n"
                + "webservices.amazon.com\n"
                + "/onca/xml\n"
                + queryString;

            return stringToHash;
        } // End of PrepareForHashing

        // PrepareQueryString
        // This method:
        //  - adds missing parameters to the query map;
        //  - sorts the map by byte value;
        //  - encodes the parameters and values;
        //  - joins the encoded parameter-value pairs to a query string.
        public string PrepareQueryString(Dictionary<string, string> queryMap)
        {
            AddMissingParameters(queryMap);

            SortedDictionary<string, string> sortedQueryMap = new SortedDictionary<string, string>(queryMap, StringComparer.Ordinal);

            string preparedQueryString = "";
            foreach (KeyValuePair<string, string> pair in sortedQueryMap)
            {
                string encodedKey = Uri.EscapeDataString(pair.Key);
                string encodedValue = Uri.EscapeDataString(pair.Value);
                preparedQueryString += "&" + encodedKey + "=" + encodedValue;
            }

            // Lets exclude the first ampersand
            return preparedQueryString.Substring(1);
        } // End of PrepareQueryString

        // AddMissingParameters
        public void AddMissingParameters(Dictionary<string, string> queryMap)
        {
            queryMap["AssociateTag"] = ASSOCIATE_TAG;
            queryMap["AWSAccessKeyId"] = AWS_ACCESS_KEY_ID;
            queryMap["Timestamp"] = GetTimestamp();
            
        }

        // GetTimeStamp
        // This method:
        //  - gets the timestamp in the correct format for Amazon PA API
        public string GetTimestamp()
        {
            DateTime currentTime = DateTime.UtcNow;
            string timestamp = String.Format("{0:yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'}", currentTime);
            return timestamp;
        } // End of GetTimestamp

    }
}